// Including the main package
package main

// Importing fmt
import "fmt"

// Defining function

func reverseArray(actArr []int, actsize int) {
    revArr := make([]int, actsize)
    j := 0
    for i := actsize - 1; i >= 0; i-- {
        revArr[j] = actArr[i]
        j++
    }

    fmt.Println("\nThe Result of a Reverse Array = ", revArr)
}
// Calling main
func main() {
    //declaring variables
    var actsize, i int      

    fmt.Print("Enter the Even Array Size = ")
    fmt.Scan(&actsize)								//Calling Scan() function for scanning and reading the input texts given in standard input

    actArr := make([]int, actsize)					// make()

    fmt.Print("Enter the Even Array Items  = ")
    for i = 0; i < actsize; i++ {
        fmt.Scan(&actArr[i])
    }

    reverseArray(actArr, actsize)
}



//PROBLEM STATEMENT:-

//In an array actarr of size actsize 

//Reverse an array of integers.

// SAMPLE INPUT-

//"Enter the Even Array Size = "  5
//Enter the Even Array Items  = 2,5,6,7,7





//SAMPLE OUTPUT-
//The Result of a Reverse Array =  [7 7 6 5 2]
