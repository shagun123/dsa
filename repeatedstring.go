
//Date- 27th september


package main

import (

    "fmt"                                  // package importing
    
)



func main() {
    var (
        n int                          // declaration of variables
        s string
    )
    fmt.Scan(&s, &n)                     // taking input
    var l int = len(s)                   //  new variable storing length of string 

    times := n / l                        
    itr := n % l

    count := 0

    for i := 0; i < l; i++ {
        if s[i] == 'a' {
            count++
        }
    }

    count = count * times

    for i := 0; i < itr; i++ {
        if s[i] == 'a' {
            count++
        }
    }
    fmt.Println(count)                               //output of the frequency of a in the substring//
}


//sample input-
// aba
// 4




//sample output-
 // 3
