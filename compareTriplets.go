package main

import "fmt"

func main() {
	var a [3]int     //array of alice, 3 elemnts for 3  categories
	var b [3]int     //array of Bob

	//Input points on a scale from 1 to 100 for three categories: problem clarity, originality, and difficulty.

	fmt.Scan(&a[0], &a[1], &a[2])       //Input elements for alice
	fmt.Scan(&b[0], &b[1], &b[2])       //input elements for Bob 
	
	//defining counter variables

	var ar, br int

	for i := 0; i < 3; i++ {
		//compare here

		if a[i] > b[i] {
			ar++
		} else if b[i] > a[i] {
			br++
		}
	}
	fmt.Println(ar, br)
}

// Sample input-
//5 6 7
//3 6 10

//Sample output-
// 1 1
