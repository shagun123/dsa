// day 3rd october
  
package main

import (
    "fmt"
)

func main() {

    var n, m int
    fmt.Scan(&n, &m)       //input for size of arrays

    a := make([]int, n)      //defining arrays
    b := make([]int, m)      

    for i := 0; i < n; i++ {
        fmt.Scan(&a[i])          // loop for taking input of array
    }
    for j := 0; j < m; j++ {
        fmt.Scan(&b[j])
    }

    var c int                           // counter for reading number of integers

    for i := 1; i <= 100; i++ {
        factor := true
        for j := 0; j < n; j++ {
            if i%a[j] != 0 {
                factor = false
                break
            }
        }
        if factor {
            for j := 0; j < m; j++ {
                if b[j]%i != 0 {
                    factor = false
                    break
                }
            }
        }
        if factor {
            c++
        }
    }
    fmt.Println(c)
}

//sample input-
2 3
2 4
16 32 96

//sample output-
3