package main

import "fmt"

func main(){
	var p,n,z,num,ar int
    // p,n,z are counter variables for positive,negative,zero respectively
	// num is the array size and ar is the array itself
    
	// input array size 
	fmt.Scan(&num)
	
	// loop to take input of array

	for i := 0; i < num; i++ {
		fmt.Scan(&ar)    

		if ar > 0 {
			p++                  // for counting positive elements
		
		} else if ar < 0 {
				n++             // for negative elements
			} else {
				z++             // for zero values
			}
		}
		
		// now want to return ratio of positive,negative and zero values
		// mainly upto six decimal places

	pf := float64(p) / float64(num)
	nf := float64(n) / float64(num)
	zf := float64(z) / float64(num)


	fmt.Printf("%.6f\n", pf)
	fmt.Printf("%.6f\n", nf)
	fmt.Printf("%.6f\n", zf)

}


