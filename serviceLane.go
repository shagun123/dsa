package main

import (
	"fmt"
)


func main() {
  var n, t int  
 //n is the length of highway, t is number of test cases
  //fmt.Println("enter the length of highway :")
  fmt.Scan(&n, &t)

  w := make([]int, n)     // width array of size n

  for i := 0; i < n; i++ {
	  fmt.Scan(&w[i])           // input array w
  }

  for t > 0 {
	  i, j := 0, 0
	  fmt.Scan(&i, &j)
	  min := w[i]
	  for i <= j {
		  if w[i] < min {
			  min = w[i]
		  }
		  i++
	  }

	  fmt.Println(min)
	  t--
  }
}