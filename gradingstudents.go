// 30th september

package main

import (
    "fmt"
)





func main() {

    var n, gr int
    fmt.Scan(&n)         // n denotes number of students 
	                    // gr as grades of students

    for i := 0; i < n; i++ {
        fmt.Scan(&gr)
        if gr < 38 {
            fmt.Println(gr)
        } else {
            if gr%5 > 2 {
                fmt.Println(((gr / 5) + 1) * 5)
            } else {
                fmt.Println(gr)
            }
        }
    }
}



// sample input-

// 4
// 73
// 67
// 8
// 33



// sample output

//75
//67
//40
//33
